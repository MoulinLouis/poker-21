# Poker 21 - Groupe 18

## Description

Projet Javascript dans le cadre de notre 4ème année en Ingénierie du Web à l'ESGI.

## Rules of the game

Poker 21 is a card game that consists of getting as close as possible to a score of 21, which is obtained by adding up the scores of each card that is drawn. The player wins if he reaches 21, or if he decides to stop and the next card is higher than 21. Each card is worth its value in points, except for the Ace which is worth 0 points, and the King, Queen and Jack which are worth 10 points. the jack, which are worth 10 points each.

## Getting started

run

```bash
docker-compose up -d
```

go to [localhost:8083](http://localhost:8082)

## What's possible in the game ?

- **Shuffle a new deck** ("*Mélanger un nouveau deck*" button)
  - A deck of 52 cards comes from nowhere
  - Deck is animated by a random shuffling animation

- **Next hand** ("*Prochaine main*" button)
  - Let the player reset his hand whenever he wants
    - Needs a least one card to be draw
    - Animation of drawn card throw randomly

- **Draw** ("*Piocher*" button)
  - Let the player draw a card
    - A card come out of the deck with an animation
    - Score is calculated as in a normal blackjack game
      - above 21 : LOOSE
      - below 21 : KEEP PLAYING
      - equal 21 : WIN

- **Stay** ("*Rester*" button)
  - Let the player draw a final card which will decide if the game is win or loose
    - The rules are reversed, if the last card is :
      - above 21 : WIN
      - below 21 : LOOSE
      - equal 21 : WIN

- **Remaining cards**
  - Player can see the number of remaining cards in the deck
  - If the remaining cards goes to 0, the player can't play anymore

- **User message**
  - The system always show a message to the user so that he can totally understand what is going on in the game (currently in french)

## What's done ?

- [x]  Il doit être possible de tirer une carte depuis un deck.
- [x]  Le jeu de 52 cartes doit être construit en utilisant l’API [https://deckofcardsapi.com](https://deckofcardsapi.com/).
- [x]  Une partie doit se dérouler avec le même jeu de 52 cartes.
- [x]  Les cartes tirées doivent être tirées depuis le même jeu de 52 cartes à chaque fois.
- [x]  Le jeu de 52 cartes ne doit contenir aucun joker.
- [x]  Chaque carte tirée doit être affichée à l’écran.
- [x]  Il ne doit pas être possible de tirer une carte après une victoire.
- [x]  Il doit être possible d’afficher le score en cours, mis à jour après chaque tirage.
- [x]  Il doit être possible de décider de s’arrêter pour voir si la carte suivante nous fait gagner ou perdre.
- [x]  Il doit être possible de pouvoir rejouer une partie après une victoire ou un échec.
- [x]  Il doit être possible de recommencer une nouvelle partie pendant une partie en cours.
- [x]  Il ne doit pas être possible de pouvoir recommencer avant d’avoir tiré au moins une carte.
- [x]  Il doit y avoir des animations affichées lorsqu’une carte est tirée et posée sur le tas.
- [x]  Il doit être possible de voir une icône affichée dans l’onglet du navigateur.
- [x]  Il doit être possible de voir un titre correctement affiché dans l’onglet du navigateur.
- [x]  Il doit être possible d’afficher le nombre de cartes restantes, mis à jour après chaque tirage.
- [x]  Il ne doit pas être possible de tirer une carte après que le jeu de carte soit vide.
- [x]  Il doit y avoir une animation des cartes lorsque le joueur gagne une partie.
- [x]  Il doit y avoir une animation des cartes lorsque le joueur perd une partie.
- [x]  Il ne doit pas être possible de tirer une nouvelle carte si une carte est déjà en cours de tirage.
- [x]  Il ne doit pas être possible de s’arrêter de tirer une carte si aucune carte n’est en cours de tirage.
- [x]  Il doit y avoir une interface graphique pensée pour l’UX/UI et agréable à utiliser. BOF BOF
- [x]  Il doit être possible de voir l’état courant du réseau (connecté, déconnecté).
- [x]  Il doit être possible de tirer une carte en appuyant sur la touche “D” du clavier.

## What's need to be done ?

- [ ]  Il doit être possible d’afficher une fenêtre modale de résultat après une victoire ou un échec.
- [ ]  Il doit être possible d’annuler le tirage d’une carte en appuyant sur la touche “C” du clavier. —
- [ ]  Il doit être possible de reprendre une partie à tout moment, même après avoir fermé l’onglet.
- [ ]  Il doit être possible de jouer à la fois sur mobile, tablette ou ordinateur.
- [ ]  Les cartes doivent être affichées les unes sur les autres de manière à imiter un paquet désordonné.—
- [ ]  Il doit être possible de tirer un nombre de cartes arbitraire à la fois.—
- [ ]  Il doit y avoir un retour haptique (vibreur) lorsqu’une carte est tirée, lors d’une victoire ou d’un échec.
- [ ]  Il doit être possible de tirer une nouvelle carte lorsque la main est proche du capteur de proximité.
- [ ]  Timeout sur un drawCard si trop long
- [ ]  Il doit être possible d’avoir un message d’erreur lorsqu’une erreur survient lors d’un tirage de carte.
- [ ]  Il doit être possible de pouvoir tirer de nouveau une carte lorsqu’une erreur réseau survient.
- [ ]  Il doit être possible d’annuler le tirage d’une carte en cours de requête.
- [ ]  Il doit être possible d’afficher une fenêtre modale de résultat après une victoire ou un échec.
- [ ]  Il doit être possible de tirer une nouvelle carte en secouant l’appareil. **PAS A FAIRE**
