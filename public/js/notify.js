class NotifyCenter {
    constructor(options = {}) {
        this.options = options;

        this.notificationContainer = null;
    }

    init(container = null) {
        if (!container) {
            container = document.body;
        }
        container.prepend(this.build());
        return this;
    }

    build() {
        let container = document.createElement('div');
        container.classList.add('notifyCenter');
        let notificationContainer = document.createElement('div');
        notificationContainer.classList.add('notificationContainer');
        if (this.options?.distance) {
            let directions = ['top', 'right', 'bottom', 'left'];
            for (let index = 0; index < directions.length; index++) {
                const direction = directions[index];
                notificationContainer.style[direction] = this.options.distance[direction];
            } 
        }

        this.notificationContainer = notificationContainer;
        container.append(this.notificationContainer);
        return container;
    }

    notify(text, status = 'neutral', timing = 5000, options = {}) {
        let notification = new Notification(text, status, timing, options);
        this.notificationContainer.append(
            notification.build()
        );
    }
}

class Notification {
    constructor(text, status = 'neutral', timing = 5000, options = {}) {
        this.text = text;
        this.status = status;
        this.timing = timing;
        this.options = options;
    }

    build() {
        let notification = document.createElement('div');
        notification.classList.add('notification', 'hidden', this.status);

        let notificationStatus = document.createElement('div');
        notificationStatus.classList.add('notificationStatus');
        let statusIcon = '';
        switch (this.status) {
            case 'success':
                statusIcon = 'fa fa-check';
                break;
            case 'error':
                statusIcon = 'fa fa-warning';
                break;
            default:
                statusIcon = 'fa fa-commenting';
                break;
        }
        notificationStatus.innerHTML = '<i class="' + statusIcon + '"></i>'

        let notificationText = document.createElement('div');
        notificationText.classList.add('notificationContent');
        notificationText.innerHTML = this.text;

        let notificationClose = document.createElement('div');
        notificationClose.classList.add('notificationClose');
        notificationClose.innerHTML = '<i class="fa fa-close"></i>';

        notification.append(
            notificationStatus,
            notificationText,
            notificationClose
        );

        this.notification = notification;
        let notificationContext = this;
        
        setTimeout(function() {
            notificationContext.notification.classList.remove('hidden');
        }, 10);
        setTimeout(function() {
            notificationContext.close(notificationContext.notification);
        }, this.timing);
        
        
        notificationClose.addEventListener('click', function() {
            notificationContext.close(notificationContext.notification);
        });

        return notification;
    }

    close(notification) {
        notification.classList.add('hidden');
        if (notification) {
            setTimeout(function() {
                notification.remove();
            }, 500);
        }
    }
}

