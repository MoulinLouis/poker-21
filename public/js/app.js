let deckId
let playerHand = []
let playerScore = 0
let roundWin = false
let roundLost = false
let deckCount

let deckExist = false
let keyEventDisabled = false
let isStay = false

let drawBtn = document.getElementById("draw")
let newHandBtn = document.getElementById("change-hand")
let stayBtn = document.getElementById("stay")
let shuffleBtn = document.getElementById("shuffle")
let deckNode = document.getElementById("deck")

let playerScoreElement = document.getElementById("player-score")
let playerMessage = document.getElementById("player-message")
let deckCountElement = document.getElementById("deck-count")

let isRotating = false

drawBtn.onclick = drawCard
newHandBtn.onclick = resetGame
stayBtn.onclick = stay

window.addEventListener('keydown', (event) =>  {
    if(keyEventDisabled === false) {
        if(event.key == "d" || event.key == "D") drawCard(); return

    }
});

function disableAllActions(isDisabled) {
    keyEventDisabled = isDisabled
    drawBtn.disabled = isDisabled
    newHandBtn.disabled = isDisabled
    stayBtn.disabled = isDisabled
    shuffleBtn.disabled = isDisabled
}

async function drawCard() {
    if (roundLost || roundWin) { playerMessage.textContent = "Vous ne pouvez plus tirer de carte."; return }
    if (deckCount === 0) { playerMessage.textContent = "Il n'y a plus de carte dans le paquet."; return }
    if (deckExist === false) { playerMessage.textContent = "Veuillez mélanger un paquet de carte."; return }

    disableAllActions(true)
    animateLastCardFromDeck()
    await timer(1000);

    fetch(`https://deckofcardsapi.com/api/deck/${deckId}/draw/?count=1`)
        .then(res => res.json())
        .then(res => {
            newCard = res.cards[0]
            playerHand.push(newCard)
            let cardImgElement = document.createElement("img");
            cardImgElement.src = newCard.image;
            cardImgElement.className = "card-image"
            deckNode.append(cardImgElement)

            playerScore = calculateScore(playerHand)
            playerScoreElement.textContent = playerScore

            deckCount-=1
            deckCountElement.textContent = deckCount

            if(isStay == false) {
                playerMessage.textContent = "Vous avez tiré un " + allCards[newCard.code]
                if (playerScore > 21) {
                    roundLost = true;
                    playerMessage.textContent = "C'est perdu !"
                    animateWinLoose("loose")
                } else if(playerScore === 21) {
                    roundWin = true;
                    playerMessage.textContent = "21... c'est gagné !"
                    animateWinLoose("win")
                }
            } else {
                if(playerScore >= 21) {
                    roundWin = true;
                    playerMessage.textContent = "Bien joué, vous avez gagné !"
                    animateWinLoose("win")
                } else {
                    roundLost = true;
                    playerMessage.textContent = "Oups... c'est perdu"
                    animateWinLoose("loose")
                }
                isStay = false
            }
        })
        .catch(console.log)
        disableAllActions(false)

}

async function animateWinLoose(winLoose) {
    disableAllActions(true)

        let notifyCenter = new NotifyCenter({distance: {
            'top': '55px'
        }}).init();

        if(winLoose == "win") {
            notifyCenter.notify('Vous avez gagné !', 'success');

        } else {
            notifyCenter.notify('Vous avez perdu !', 'error');
        }

    await timer(1000)

    let cardsImage = document.getElementsByClassName("card-image")

    for (var i = (cardsImage.length - 1); i >= 0; i--) {
        cardsImage[i].style.transform = "translate(0px, " + 40 * i + "px)"
        await timer(200)
    }
    await timer(1000)

    for (var i = (cardsImage.length - 1); i >= 0; i--) {
        cardsImage[i].style.transform = "translate(0px, -" + 40 * i + "px)"
        await timer(200)
    }
    await timer(1000)
    for (var i = (cardsImage.length - 1); i >= 0; i--) {
        cardsImage[i].style.transform = "translate(0px, 0px)"
        await timer(200)
    }
    disableAllActions(false)
}

async function stay() {
    if (roundLost || roundWin) { playerMessage.textContent = "Vous ne pouvez plus tirer de carte."; return }
    if (playerHand.length < 1) { playerMessage.textContent = "Vous devez tirer au moins une carte"; return }

    disableAllActions(true)
    isStay = true
    playerMessage.textContent = "Attention..."
    await timer(1000)
    playerMessage.textContent = "Si c'est au dessus de 21, c'est gagné..."
    await timer(1500)
    playerMessage.textContent = "Si c'est en dessous de 21, c'est perdu..."
    await timer(1500)
    drawCard()
}

function resetGame() {
    if (playerHand.length < 1) { playerMessage.textContent = "Vous devez tirer au moins une carte"; return }

    playerMessage.textContent = "On jète les cartes..."
    playerHand = []
    roundLost = false
    roundWin = false
    playerScore = 0
    resetCardsToDeck("Votre main a été réinitialisée.")
}



async function resetCardsToDeck(textToShow) {
    disableAllActions(true)

    let cardsImage = document.getElementsByClassName("card-image")
    if(cardsImage) {
        for (var i = (cardsImage.length - 1); i >= 0; i--) {
            if(i%2 != 0) {
                cardsImage[i].style.transform = "translate(-500px, 1000px)"
            } else {
                cardsImage[i].style.transform = "translate(500px, -1000px)"
            }
            await timer(500)
            cardsImage[i].remove()
        }
        playerMessage.textContent = textToShow
        playerScoreElement.textContent = playerScore
    }
    disableAllActions(false)


}

function calculateScore(cards) {
    score = cards.reduce((acc, card) => {
        if (card.value === "ACE") {
            return acc
        }
        if (isNaN(card.value)) { return acc + 10 }
        return acc + Number(card.value);
    }, 0)
    return score
}

async function animateLastCardFromDeck() {
    let cards = document.getElementsByClassName("card");
    let lastCard = cards[cards.length-1]
    lastCard.style.transform = "translate(" + 140 * (playerHand.length + 1) +  "px,0px)"
    await timer(1000)
    lastCard.remove()
}

function rotateContainer() {
    if(isRotating == false) {
        document.getElementById('rotating_div').style.animationPlayState='running'
        isRotating = true
    } else if(isRotating == true) {
        document.getElementById('rotating_div').style.animationPlayState='paused'
        isRotating = false
    }
}

const timer = ms => new Promise(res => setTimeout(res, ms))