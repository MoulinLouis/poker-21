const wrapper = document.querySelector(".wrapper"),
toast = wrapper.querySelector(".toast"),
title = toast.querySelector("span"),
subTitle = toast.querySelector("p"),
wifiIcon = toast.querySelector(".icon"),
closeIcon = toast.querySelector(".close-icon");
let isOnline;
window.onload = ()=>{
    function ajax(){
        let xhr = new XMLHttpRequest();
        xhr.open("GET", "https://jsonplaceholder.typicode.com/posts", true);
        xhr.onload = ()=>{
            if(xhr.status == 200 && xhr.status < 300){
                toast.classList.remove("offline");
                title.innerText = "Vous êtes en ligne";
                subTitle.innerText = "Youpi! Internet est là.";
                wifiIcon.innerHTML = '<i class="uil uil-wifi"></i>';
                closeIcon.onclick = ()=>{
                    wrapper.classList.add("hide");
                }
                setTimeout(()=>{
                    wrapper.classList.add("hide");
                }, 5000);
                isOnline = true
            }else{
                offline();
            }
        }
        xhr.onerror = ()=>{
            offline();
        }
        xhr.send();
    }
    function offline(){
        isOnline = false
        wrapper.classList.remove("hide");
        toast.classList.add("offline");
        title.innerText = "Vous êtes hors ligne.";
        subTitle.innerText = "Attention! Internet est déconnecté.";
        wifiIcon.innerHTML = '<i class="uil uil-wifi-slash"></i>';
    }
    setInterval(()=>{
        ajax();
    }, 5000);
}