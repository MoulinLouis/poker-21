shuffleBtn.onclick = shuffleDeck

async function shuffleDeck() {
    disableAllActions(true)
    if(deckExist) {
        resetGame()
    }
    playerMessage.textContent = "Mélange en cours..."
    getNewDeck()
    let cards = document.getElementsByClassName("card");
    for (var i = (cards.length - 1); i >= 0; i--) {
        cards[i].remove()
    }
    for(i = 52; i > 0; i--) {
        deckNode.prepend(getCardElement(i))
    }
    animationShuffle()
    deckExist = true
}

// When shuffle deck is clicked
async function animationShuffle() {
    let cards = document.getElementsByClassName("card");
    let animationCount = 5
    // We call the animation of shuffle 10 times
    for(j = 0; j < animationCount; j++) {
        animateCards(cards, 3, 3)
        await timer(500)
    }
    await timer(250)
    playerMessage.textContent = "Mélange terminé."
    disableAllActions(false)
}

// Move each card randomly between +10px or -10px on x and y axes
async function animateCards(cards, xMax, yMax) {
    await timer(500)
    x = 0
    y = 0
    for (var i = (cards.length - 1); i >= 0; i--) {
        x += Math.random() < 0.5 ? -xMax : +xMax
        y += Math.random() < 0.5 ? -yMax : yMax
        cards[i].style.transform = "translate(" + x + "px," + y +"px)"
    }
    await timer(500)

    for (var i = (cards.length - 1); i >= 0; i--) {
        cards[i].style.transform = "translate(0px,0px)"
    }
    await timer(100)
}

function getCardElement(zindex) {
    let div = document.createElement("div")
    div.className = "card"
    div.style.transform = "translate(0px, 0px)"
    div.style.cursor = "move"
    div.style.zIndex = zindex
    let div2 = document.createElement("div")
    div2.className = "back"
    div.append(div2)
    return div
}